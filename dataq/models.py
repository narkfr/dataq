from django.db import models
from django.utils import timezone

from devices import models as devices_models


# Create your models here.
class Data(models.Model):

    date = models.DateTimeField(default=timezone.now)

    value = models.IntegerField()

    device = models.ForeignKey(
        to=devices_models.Device,
        on_delete=models.PROTECT,
    )

    label = models.CharField(
        max_length=250,
        blank=True,
        null="",
    )

    dataType = models.ForeignKey(
        to='DataType',
        on_delete=models.PROTECT,
    )

    def __str__(self):
        return str(self.value)


class DataType(models.Model):

    name = models.CharField(
        max_length=250,
    )

    unit = models.CharField(
        max_length=10,
    )

    def __str__(self):
        return self.name
