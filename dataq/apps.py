from django.apps import AppConfig


class DataqConfig(AppConfig):
    name = 'dataq'
