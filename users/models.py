from django.db import models

from django.contrib.auth.models import User
from django.contrib.postgres.fields import JSONField


# Create your models here.
class DataQUser(models.Model):

    user = models.OneToOneField(User, on_delete=models.CASCADE)

    config = JSONField()

    def __str__(self):
        return self.user.email
